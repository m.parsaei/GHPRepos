

package git.popular.repos.viewmodel

import androidx.lifecycle.ViewModel
import git.popular.repos.model.repository.GitRepoRepository
import javax.inject.Inject

/**
 * The ViewModel used in [GitRepoFragment].
 */
class GitRepoViewModel @Inject constructor(private val repository: GitRepoRepository) : ViewModel() {

    lateinit var id: String

    val gitRepos by lazy {
        repository.getGitRepo(id)
    }

}
