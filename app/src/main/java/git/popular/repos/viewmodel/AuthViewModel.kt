package git.popular.repos.viewmodel

import android.content.SharedPreferences
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import git.popular.repos.view.callback.OnAuthListener
import git.popular.repos.util.default
import git.popular.repos.util.isEmail
import javax.inject.Inject


class AuthViewModel @Inject constructor(private val sharedPreferences: SharedPreferences
) : ViewModel() {

    companion object {
        const val EMAIL = "email"
        const val PASSWORD = "pass"
    }

    var email: String = ""
    var password: String = ""
    var confirmPassword: String = ""

    var emailError = MutableLiveData<String>()
    var passwordError = MutableLiveData<String>()
    var passwordConfirmError = MutableLiveData<String>()

    var isUerRegistered = MutableLiveData<Boolean>().default(false)

    var emailRequestFocus = MutableLiveData<Boolean>()
    var passwordRequestFocus = MutableLiveData<Boolean>()
    var confirmPasswordRequestFocus = MutableLiveData<Boolean>()

    var listener: OnAuthListener? = null

    init {
        val storedEmail = sharedPreferences.getString(EMAIL, null)
        isUerRegistered.value = (storedEmail != null) // For hide "finger print" first time
    }

    fun onSignIn(view: View) {
        listener?.onAuthStarted()

        if (isDataValid(true)) {

            val storedEmail = sharedPreferences.getString(EMAIL, null)
            storedEmail?.let {
                val storedPassword = sharedPreferences.getString(PASSWORD, "")
                if ((email == storedEmail) && (password == storedPassword))
                    listener?.onAuthSuccess()
                else
                    listener?.onAuthFailure("Error: please check your data.")
                return
            }
            listener?.onAuthFailure("User is not exist, please Register.")

        } else {
            listener?.onAuthFailure("Sign In failed! please, check data.")
        }
    }

    fun onSignUp(view: View) {
        listener?.onAuthStarted()

        if (isDataValid(false)) {
            sharedPreferences.edit().putString(EMAIL, email).commit()
            sharedPreferences.edit().putString(PASSWORD, password).commit()
            isUerRegistered.value = true
            listener?.onAuthSuccess()
        }
    }

    private fun isDataValid(isSignInOperation: Boolean): Boolean {
        when {
            email.isEmpty() -> {
                emailError.setValue("Required!")
                emailRequestFocus.value = true
            }
            !email.isEmail() -> {
                emailError.value = "Invalid!"
                emailRequestFocus.value = true
            }
            password.isEmpty() -> {
                passwordError.value = "Required!"
                passwordRequestFocus.value = true
            }
            isSignInOperation -> return true
            password != confirmPassword -> {
                passwordConfirmError.value = "Do not match."
                confirmPasswordRequestFocus.value = true
            }
            else -> return true
        }
        return false
    }
}
