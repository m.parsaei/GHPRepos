package git.popular.repos.viewmodel

import androidx.lifecycle.ViewModel
import git.popular.repos.base.di.CoroutineScropeIO
import git.popular.repos.model.repository.GitRepoRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

/**
 * The ViewModel for [GitReposFragment].
 */
class GitReposViewModel @Inject constructor(private val repository: GitRepoRepository,
                                            @CoroutineScropeIO private val ioCoroutineScope: CoroutineScope)
    : ViewModel() {

    var connectivityAvailable: Boolean = false

    val gitRepos by lazy {
        repository.getPagedGitRepos(
                connectivityAvailable, ioCoroutineScope)
    }

    /**
     * Cancel all coroutines when the ViewModel is cleared.
     */
    override fun onCleared() {
        super.onCleared()
        ioCoroutineScope.cancel()
    }

}
