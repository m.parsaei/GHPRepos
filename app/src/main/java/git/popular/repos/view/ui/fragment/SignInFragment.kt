package git.popular.repos.view.ui.fragment

import android.content.Intent
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import git.popular.repos.R
import git.popular.repos.base.biometrix.BiometricCallback
import git.popular.repos.base.biometrix.BiometricManager
import git.popular.repos.base.di.Injectable
import git.popular.repos.base.di.injectViewModel
import git.popular.repos.base.security.SecureLocalManager
import git.popular.repos.databinding.FragmentSignInBinding
import git.popular.repos.model.entity.User
import git.popular.repos.util.CryptoTool
import git.popular.repos.util.KeyboardHandler
import git.popular.repos.util.show
import git.popular.repos.view.callback.OnAuthListener
import git.popular.repos.view.ui.activity.MainActivity
import git.popular.repos.viewmodel.AuthViewModel
import javax.crypto.Cipher
import javax.inject.Inject

class SignInFragment : Fragment(), Injectable, View.OnClickListener, OnAuthListener, BiometricCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: AuthViewModel


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSignInBinding.inflate(inflater, container, false)
        context ?: return binding.root

        viewModel = injectViewModel(viewModelFactory)
        viewModel.listener = this
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        binding.btnSignUp.setOnClickListener(this)


        binding.btnFingerPrint.show()
        binding.btnFingerPrint.setOnClickListener(this)


        setHasOptionsMenu(true)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        CryptoTool.secureLocalManager = SecureLocalManager(activity!!)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSignUp -> {
                val direction = SignInFragmentDirections.actionSigninToSignup()
                v.findNavController().navigate(direction)
            }
            R.id.btnFingerPrint -> {
                authenticate()
            }
        }
    }

    override fun onAuthStarted() {
        activity!!.currentFocus?.let { KeyboardHandler.hideKeyboard(activity!!, it) }
    }

    override fun onAuthSuccess(user: User?) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        activity?.startActivity(intent)
    }

    override fun onAuthFailure(message: String) =
            Snackbar.make(view!!, message, Snackbar.LENGTH_LONG).show()

    fun authenticate() {
        val cipher = CryptoTool.secureLocalManager.getLocalEncryptionCipher()

        cipher?.let {
            BiometricManager.BiometricBuilder(activity)
                    .setTitle("Authorise")
                    .setSubtitle("Please, authorise yourself")
                    .setDescription("This is needed to perform cryptographic operations.")
                    .setNegativeButtonText("Cancel")
                    .setCipher(it)
                    .build()
                    .authenticate(this)
        }
    }

    private fun biometricSignIn(cipher: Cipher) {
        CryptoTool.secureLocalManager.loadOrGenerateApplicationKey(cipher)
        CryptoTool.isBiometricAuthenticated = true
        onAuthSuccess()
    }

    override fun onResume() {
        super.onResume()
        CryptoTool.isBiometricAuthenticated = false
    }

    override fun onSdkVersionNotSupported() {
    }

    override fun onBiometricAuthenticationPermissionNotGranted() {
    }

    override fun onAuthenticationCancelled() {
    }

    override fun onBiometricAuthenticationInternalError(error: String?) {
    }

    override fun onBiometricAuthenticationNotSupported() {
    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
    }

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
    }

    override fun onAuthenticationSuccessful(result: FingerprintManagerCompat.AuthenticationResult?) {
        val cipher = result!!.cryptoObject.cipher!!
        biometricSignIn(cipher)    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onAuthenticationSuccessful(result: BiometricPrompt.AuthenticationResult?) {
        val cipher = result!!.cryptoObject.cipher!!
        biometricSignIn(cipher)
    }

    override fun onAuthenticationFailed() {
    }

    override fun onBiometricAuthenticationNotAvailable() {
    }

}