package git.popular.repos.view.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.biometric.BiometricManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import git.popular.repos.view.ui.activity.MainActivity
import git.popular.repos.view.callback.OnAuthListener
import git.popular.repos.model.entity.User
import git.popular.repos.base.di.Injectable
import git.popular.repos.base.di.injectViewModel
import git.popular.repos.util.KeyboardHandler
import git.popular.repos.viewmodel.AuthViewModel
import com.google.android.material.snackbar.Snackbar
import git.popular.repos.databinding.FragmentSignUpBinding
import javax.inject.Inject


class SignUpFragment : Fragment(), Injectable, OnAuthListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: AuthViewModel
    private lateinit var biometricManager: BiometricManager


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSignUpBinding.inflate(inflater, container, false)
        context ?: return binding.root

        viewModel = injectViewModel(viewModelFactory)
        viewModel.listener = this
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this


        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onAuthStarted() {
        activity!!.currentFocus?.let { KeyboardHandler.hideKeyboard(activity!!, it) }
    }

    override fun onAuthSuccess(user: User?) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or  Intent.FLAG_ACTIVITY_CLEAR_TASK

        activity?.startActivity(intent)
    }

    override fun onAuthFailure(message: String) {
        Snackbar.make(view!!, message, Snackbar.LENGTH_LONG).show()
    }


}