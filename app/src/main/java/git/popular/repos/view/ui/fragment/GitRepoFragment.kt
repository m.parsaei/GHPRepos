package git.popular.repos.view.ui.fragment

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import git.popular.repos.R
import git.popular.repos.base.di.Injectable
import git.popular.repos.base.di.injectViewModel
import git.popular.repos.databinding.FragmentGitRepoBinding
import git.popular.repos.model.db.Result
import git.popular.repos.model.entity.GitRepo
import git.popular.repos.util.binding.bindImageFromUrl
import git.popular.repos.util.hide
import git.popular.repos.util.intentShareText
import git.popular.repos.util.setTitle
import git.popular.repos.util.show
import git.popular.repos.viewmodel.GitRepoViewModel
import javax.inject.Inject

/**
 * A fragment representing a single GitHub Repository detail screen.
 */
class GitRepoFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: GitRepoViewModel

    private val args: GitRepoFragmentArgs by navArgs()
    private lateinit var mGitRepo: GitRepo

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        viewModel = injectViewModel(viewModelFactory)
        viewModel.id = args.id

        val binding = DataBindingUtil.inflate<FragmentGitRepoBinding>(
                inflater, R.layout.fragment_git_repo, container, false).apply {
            lifecycleOwner = this@GitRepoFragment
        }

        subscribeUi(binding)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_share, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @Suppress("DEPRECATION")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_share -> {
                intentShareText(activity!!, getString(R.string.share_git_repo, mGitRepo.name, mGitRepo.htmlUrl))
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun subscribeUi(binding: FragmentGitRepoBinding) {
        viewModel.gitRepos.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    binding.progressBar.hide()
                    result.data?.let { bindView(binding, it) }
                }
                Result.Status.LOADING -> binding.progressBar.show()
                Result.Status.ERROR -> {
                    binding.progressBar.hide()
                    Snackbar.make(binding.coordinatorLayout, result.message!!, Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun bindView(binding: FragmentGitRepoBinding, gitRepo: GitRepo) {
        gitRepo.apply {
            setTitle(name)
            binding.id.text = "Id: $id"
            binding.language.text = "Language: $language"
            binding.name.text = "Name: $name"
            binding.desc.text = "Description: $description"
            binding.starsCount.text = stargazersCount.toString()
            binding.watchesCount.text = watchersCount.toString()
            val type = if (isPrivate) "PRIVATE" else "PUBLIC"
            binding.type.text = "Type: $type"
            binding.ownerId.text = "Owner Id: ${owner.ownerId}"
            binding.owner.text = "Owner: ${owner.login}"
            bindImageFromUrl(binding.ownerAvatar, owner.avatarUrl)

            mGitRepo = gitRepo
        }
    }
}
