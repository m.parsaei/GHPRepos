package git.popular.repos.view.ui.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import git.popular.repos.base.di.Injectable
import git.popular.repos.base.di.injectViewModel
import git.popular.repos.base.worker.SendNotificationWorker
import git.popular.repos.databinding.FragmentGitReposBinding
import git.popular.repos.model.db.Result
import git.popular.repos.view.adapter.GitRepoAdapter
import git.popular.repos.viewmodel.GitReposViewModel
import git.popular.repos.util.ConnectivityUtil
import git.popular.repos.util.hide
import git.popular.repos.util.show
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GitReposFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: GitReposViewModel

    private lateinit var binding: FragmentGitReposBinding
    private val adapter: GitRepoAdapter by lazy { GitRepoAdapter() }
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewModel = injectViewModel(viewModelFactory)
        viewModel.connectivityAvailable = ConnectivityUtil.isConnected(context!!)

        binding = FragmentGitReposBinding.inflate(inflater, container, false)
        context ?: return binding.root

        linearLayoutManager = LinearLayoutManager(activity)
        setLayoutManager()
        binding.recyclerView.adapter = adapter

        subscribeUi(adapter)

        /**
         * Show notification to update the data base every 15 minutes
         */
        val constraints = Constraints.Builder()
                .setRequiresCharging(true)
                .build()
        val work = PeriodicWorkRequestBuilder<SendNotificationWorker>(15, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .setInitialDelay(15, TimeUnit.MINUTES)
                .build()
        val workManager = WorkManager.getInstance(activity!!)

        workManager.enqueueUniquePeriodicWork("SEND_REFRESH_DB_NOTIFICATION", ExistingPeriodicWorkPolicy.REPLACE, work)
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun subscribeUi(adapter: GitRepoAdapter) {
        viewModel.gitRepos.observe(viewLifecycleOwner) {

            binding.progressBar.hide()
            adapter.submitList(it)

        }
    }

    private fun setLayoutManager() {
        val recyclerView = binding.recyclerView

        var scrollPosition = 0
        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.layoutManager != null) {
            scrollPosition = (recyclerView.layoutManager as LinearLayoutManager)
                    .findFirstCompletelyVisibleItemPosition()
        }

        recyclerView.layoutManager = linearLayoutManager

        recyclerView.scrollToPosition(scrollPosition)
    }
}
