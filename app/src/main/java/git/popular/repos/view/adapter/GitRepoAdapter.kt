package git.popular.repos.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import git.popular.repos.databinding.ListItemGitRepoBinding
import git.popular.repos.model.entity.GitRepo
import git.popular.repos.view.ui.fragment.GitReposFragmentDirections

/**
 * Adapter for the [RecyclerView] in [GitReposFragment].
 */
class GitRepoAdapter : PagedListAdapter<GitRepo, GitRepoAdapter.ViewHolder>(GitRepoDiffCallback()) {




    private lateinit var recyclerView: RecyclerView

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val gitRepo = getItem(position)
        gitRepo?.let {
            holder.apply {
                bind(createOnClickListener(gitRepo.id.toString()), gitRepo)
                itemView.tag = gitRepo
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ListItemGitRepoBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    private fun createOnClickListener(id: String): View.OnClickListener {
        return View.OnClickListener {
            val direction = GitReposFragmentDirections.actionGitReposToGitRepo(id)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(private val binding: ListItemGitRepoBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, item: GitRepo) {
            binding.apply {
                clickListener = listener
                gitRepo = item
                executePendingBindings()
            }
        }
    }
}

private class GitRepoDiffCallback : DiffUtil.ItemCallback<GitRepo>() {

    override fun areItemsTheSame(oldItem: GitRepo, newItem: GitRepo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GitRepo, newItem: GitRepo): Boolean {
        return oldItem == newItem
    }
}