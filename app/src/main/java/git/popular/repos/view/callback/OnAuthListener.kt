package git.popular.repos.view.callback

import git.popular.repos.model.entity.User

interface OnAuthListener {
    fun onAuthStarted()
    fun onAuthSuccess(user: User? =null)
    fun onAuthFailure(message: String)
}