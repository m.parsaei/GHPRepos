package git.popular.repos.base.di


import git.popular.repos.view.ui.fragment.SignInFragment
import git.popular.repos.view.ui.fragment.SignUpFragment
import git.popular.repos.view.ui.fragment.GitRepoFragment
import git.popular.repos.view.ui.fragment.GitReposFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): SignInFragment

    @ContributesAndroidInjector
    abstract fun contributeSignUpFragment(): SignUpFragment

    @ContributesAndroidInjector
    abstract fun contributeGitReposFragment(): GitReposFragment

    @ContributesAndroidInjector
    abstract fun contributeGitRepoFragment(): GitRepoFragment
}
