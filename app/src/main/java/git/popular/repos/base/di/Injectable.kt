package git.popular.repos.base.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
