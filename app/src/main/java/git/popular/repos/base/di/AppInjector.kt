package git.popular.repos.base.di

import android.app.*
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import git.popular.repos.base.security.SecureLocalManager
import git.popular.repos.App
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import git.popular.repos.base.worker.DeleteReposWorker
import git.popular.repos.util.Tools

/**
 * Helper class to automatically inject fragments if they implement [Injectable].
 */
object AppInjector {

    fun init(application: App) {
        DaggerAppComponent.builder().application(application)
                .build().inject(application)
        application

                .registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                        handleActivity(activity)
                    }

                    override fun onActivityStarted(activity: Activity) {
                        if (SecureLocalManager(activity).hasChangedBiometric()) {


                            Tools.showDialog(activity,
                                    "Security Alert", "Your biometric authentication information has been changed, your local data have to be deleted.")
                                    .setPositiveButton(android.R.string.yes) { dialog, which ->
                                        biometricChanged(activity)
                                    }
                                    .show()

                        }
                    }

                    override fun onActivityResumed(activity: Activity) {

                    }

                    override fun onActivityPaused(activity: Activity) {

                    }

                    override fun onActivityStopped(activity: Activity) {

                    }

                    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {

                    }

                    override fun onActivityDestroyed(activity: Activity) {

                    }
                })
    }

    private fun biometricChanged(activity:Activity) {

        SecureLocalManager(activity).removeKeys()

        /**
         * Delete all local data
         */
        val request = OneTimeWorkRequestBuilder<DeleteReposWorker>().build()
        WorkManager.getInstance(activity).enqueue(request)

        /**
         * Navigate for first screen
         */
        val intent = Intent()
        intent.setClassName(activity.applicationContext.packageName, "git.popular.repos.view.ui.activity.AuthActivity")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent)


    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                    .registerFragmentLifecycleCallbacks(
                            object : FragmentManager.FragmentLifecycleCallbacks() {
                                override fun onFragmentCreated(
                                        fm: FragmentManager,
                                        f: Fragment,
                                        savedInstanceState: Bundle?
                                ) {
                                    if (f is Injectable) {
                                        AndroidSupportInjection.inject(f)
                                    }
                                }
                            }, true
                    )
        }
    }
}
