package git.popular.repos.base.worker

import android.app.Notification.EXTRA_NOTIFICATION_ID
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat

import androidx.work.Worker
import androidx.work.WorkerParameters
import git.popular.repos.R
import git.popular.repos.base.receiver.UpdateDatabaseBroadcastReceiver

class SendNotificationWorker(
        context: Context,
        workerParams: WorkerParameters
) : Worker(context, workerParams) {
    override fun doWork(): Result {
        sendNotification("Update Database", "You need to update your database data.");
        return Result.success()
    }

    private fun sendNotification(title: String, message: String) {
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;

        //If on Oreo then notification required a notification channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel);
        }

        val intent = Intent(applicationContext, UpdateDatabaseBroadcastReceiver::class.java).apply {
            action = "ACTION_REFRESH_DATA"
            putExtra(EXTRA_NOTIFICATION_ID, 0)
        }
        val pendingIntent: PendingIntent =
                PendingIntent.getBroadcast(applicationContext, 0, intent, 0)
        val notification = NotificationCompat.Builder(applicationContext, "default")
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .addAction(R.drawable.ic_info_white_24dp, applicationContext.getString(R.string.update_data),
                        pendingIntent)

        notificationManager.notify(1, notification.build())

    }
}