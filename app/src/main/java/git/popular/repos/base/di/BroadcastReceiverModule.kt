package git.popular.repos.base.di

import git.popular.repos.base.receiver.UpdateDatabaseBroadcastReceiver
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module()
abstract class BroadcastReceiverModule {
    @ContributesAndroidInjector
    abstract fun contributeUpdateDatabaseBroadcastReceiver(): UpdateDatabaseBroadcastReceiver
}