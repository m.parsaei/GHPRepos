package git.popular.repos.base.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import git.popular.repos.viewmodel.AuthViewModel
import git.popular.repos.viewmodel.GitRepoViewModel
import git.popular.repos.viewmodel.GitReposViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    abstract fun bindAuthViewModel(viewModel: AuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GitReposViewModel::class)
    abstract fun bindGitReposFragment(viewModel: GitReposViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GitRepoViewModel::class)
    abstract fun bindGitRepoFragment(viewModel: GitRepoViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
