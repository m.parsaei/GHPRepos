package git.popular.repos.base.receiver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import git.popular.repos.base.worker.DeleteReposWorker
import git.popular.repos.model.repository.GitRepoRepository
import dagger.android.AndroidInjection
import javax.inject.Inject

class UpdateDatabaseBroadcastReceiver : BroadcastReceiver() {

    /**
     * If need it, we can use it
     */
    @Inject
    lateinit var repository: GitRepoRepository

    override fun onReceive(context: Context?, intent: Intent?) {
        AndroidInjection.inject(this, context)

        /**
         * Delete all local data
         */
        val request = OneTimeWorkRequestBuilder<DeleteReposWorker>().build()
        WorkManager.getInstance(context!!).enqueue(request)

        /**
         * Close the notification
         */
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(1)

        /**
         * Navigate for first screen
         */
        val intent = Intent()
        intent.setClassName(context.applicationContext.packageName, "git.popular.repos.view.ui.activity.AuthActivity")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
    }
}