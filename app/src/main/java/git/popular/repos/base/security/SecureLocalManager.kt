package git.popular.repos.base.security

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.security.keystore.KeyPermanentlyInvalidatedException
import java.lang.Exception
import javax.crypto.Cipher


class SecureLocalManager(ctxt: Context) {
    companion object {
        const val SHARED_PREFERENCES_NAME = "settings"
        const val APPLICATION_KEY_NAME = "ApplicationKey"
        const val IV_SIZE = 16
    }

    private var keystoreManager: KeystoreManager
    private var cryptoHelper: CryptoHelper
    private lateinit var applicationKey: ByteArray
    private var applicationContext: Context

    init {
        applicationContext = ctxt
        cryptoHelper = CryptoHelper()
        keystoreManager = KeystoreManager(applicationContext, cryptoHelper)
        keystoreManager.generateMasterKeys()
    }


    fun encryptLocalData(data: ByteArray): ByteArray {
        val iv = cryptoHelper.generateIV(IV_SIZE)
        return iv + cryptoHelper.encryptData(data, applicationKey, iv)
    }

    fun decryptLocalData(data: ByteArray): ByteArray {
        val iv = data.sliceArray(0..IV_SIZE - 1)
        val ct = data.sliceArray(IV_SIZE..data.lastIndex)
        return cryptoHelper.decryptData(ct, applicationKey, iv)
    }

    fun getLocalEncryptionCipher(): Cipher {
        return keystoreManager.getLocalEncryptionCipher()
    }

    fun loadOrGenerateApplicationKey(cipher: Cipher) {
        val preferences = applicationContext.getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE)
        if (preferences.contains(APPLICATION_KEY_NAME)) {
            val encryptedAppKey = preferences.getString(APPLICATION_KEY_NAME, "")!!
            applicationKey = keystoreManager.decryptApplicationKey(cryptoHelper.hexToByteArray(encryptedAppKey), cipher)
        } else {
            applicationKey = cryptoHelper.generateApplicationKey()
            val editor = preferences.edit()
            val encryptedAppKey = cryptoHelper.byteArrayToHex(keystoreManager.encryptApplicationKey(applicationKey, cipher))
            editor.putString(APPLICATION_KEY_NAME, encryptedAppKey)
            editor.apply()

        }
    }

    fun hasChangedBiometric(): Boolean {
        return try {
            keystoreManager.hasChangedBiometric()
            false
        } catch (ex: KeyPermanentlyInvalidatedException) {
            true
        } catch (ex: Exception) {
            false
        }
    }

    fun removeKeys() {
        keystoreManager.removeKeys()

        applicationContext.getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE).edit().remove(APPLICATION_KEY_NAME).commit()
    }
}