package git.popular.repos.base.di

import android.app.Application
import android.content.Context.MODE_PRIVATE
import git.popular.repos.model.api.GitService
import git.popular.repos.model.db.AppDatabase
import git.popular.repos.model.datasource.GitRepoRemoteDataSource
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, CoreDataModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideGitRepoService(okhttpClient: OkHttpClient,
                           converterFactory: GsonConverterFactory
    ) = provideService(okhttpClient, converterFactory, GitService::class.java)

    @Singleton
    @Provides
    fun provideGitRepoRemoteDataSource(service: GitService)
            = GitRepoRemoteDataSource(service)

    @Singleton
    @Provides
    fun provideGitRepoDao(db: AppDatabase) = db.gitRepoDao()

    @Singleton
    @Provides
    fun provideDb(app: Application) = AppDatabase.getInstance(app)

    @Singleton
    @Provides
    fun provideSharedPreferences(app: Application) = app.getSharedPreferences("my-shared_pref",
            MODE_PRIVATE)

    @CoroutineScropeIO
    @Provides
    fun provideCoroutineScopeIO() = CoroutineScope(Dispatchers.IO)

    private fun createRetrofit(
            okhttpClient: OkHttpClient,
            converterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
                .baseUrl(GitService.ENDPOINT)
                .client(okhttpClient)
                .addConverterFactory(converterFactory)
                .build()
    }

    private fun <T> provideService(okhttpClient: OkHttpClient,
            converterFactory: GsonConverterFactory, clazz: Class<T>): T {
        return createRetrofit(okhttpClient, converterFactory).create(clazz)
    }

}
