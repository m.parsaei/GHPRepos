package git.popular.repos.base.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import git.popular.repos.model.db.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import timber.log.Timber

class DeleteReposWorker(
        context: Context,
        workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    override suspend fun doWork(): Result = coroutineScope {
        withContext(Dispatchers.IO) {
            try{
                        AppDatabase.getInstance(applicationContext).gitRepoDao().deleteAll()
                        Result.success()
            } catch (e: Exception) {
                Timber.e(e, "Error seeding database")
                Result.failure()
            }
        }
    }
}