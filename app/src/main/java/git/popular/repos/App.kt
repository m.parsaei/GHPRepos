package git.popular.repos

import android.app.Activity
import android.app.Application
import android.content.BroadcastReceiver
import android.widget.Toast
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import git.popular.repos.base.di.AppInjector
import git.popular.repos.base.worker.SendNotificationWorker
import git.popular.repos.util.CrashReportingTree
import com.facebook.stetho.Stetho
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasBroadcastReceiverInjector
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class App : Application(), HasActivityInjector, HasBroadcastReceiverInjector {
    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var receiverInjector: DispatchingAndroidInjector<BroadcastReceiver>

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) Stetho.initializeWithDefaults(this)

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        else Timber.plant(CrashReportingTree())

        AppInjector.init(this)
    }

    override fun activityInjector() = activityInjector
    override fun broadcastReceiverInjector() = receiverInjector
}