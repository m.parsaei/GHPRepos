package git.popular.repos.util

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

object Tools {
    fun addGroupingSeparator(data: String) = String.format("%d", data)

    fun showDialog(context: Context, title: String, message: String): AlertDialog.Builder {
        // build alert dialog
        val dialogBuilder = AlertDialog.Builder(context)

        // set message of alert dialog
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
                // if the dialog is cancelable
                .setCancelable(false)


        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(title)

        return dialogBuilder

    }
}
