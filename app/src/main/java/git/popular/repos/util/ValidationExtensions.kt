package git.popular.repos.util

fun String.isEmail():Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches();
}