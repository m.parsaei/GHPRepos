package git.popular.repos.util

import android.util.Base64
import git.popular.repos.base.security.SecureLocalManager

object CryptoTool {

    lateinit var secureLocalManager: SecureLocalManager
    var isBiometricAuthenticated = false

    fun encrypt(plainData: String): String {
        return try {
            val encrypted = secureLocalManager.encryptLocalData(plainData.toByteArray())
            Base64.encodeToString(encrypted, Base64.NO_WRAP)
        } catch (ex: Exception) {
            plainData
        }
    }


    fun decrypt(encryptedData: String): String {
        return try {
            String(secureLocalManager.decryptLocalData(Base64.decode(encryptedData, Base64.NO_WRAP)))
        } catch (ex: Exception) {
            encryptedData
        }
    }
}