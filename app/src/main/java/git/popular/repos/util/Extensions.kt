package git.popular.repos.util

import androidx.lifecycle.MutableLiveData

/**
 * @author Mehdi Parsaei
 * @since 2/15/2020
 * @version 1.0.0
 * https://www.linkedin.com/in/mehdiparsaei/
 */
fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

fun String.addGroupingSeparator() = String.format("%d",this)

