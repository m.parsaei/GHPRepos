package git.popular.repos.model.api

enum class NetworkState {
    RUNNING,
    SUCCESS,
    FAILED
}