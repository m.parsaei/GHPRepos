package git.popular.repos.model.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import git.popular.repos.util.CryptoTool
import com.google.gson.annotations.SerializedName

@Entity(tableName = "gitRepos")
data class GitRepo(
        @PrimaryKey
        @field:SerializedName("id")
        val id: Int,
        @field:SerializedName("name")
        var name: String,
        @field:SerializedName("stargazers_count")
        val stargazersCount: Int,
        @field:SerializedName("node_id")
        val nodeId: String?,
        @field:SerializedName("url")
        val url: String?,
        @field:SerializedName("full_name")
        val fullName: String?,
        @field:SerializedName("private")
        val isPrivate: Boolean,
        @field:SerializedName("html_url")
        val htmlUrl: String,
        @field:SerializedName("description")
        val description: String?,
        @field:SerializedName("fork")
        val isFork: Boolean,
        @field:SerializedName("forks_url")
        val forksUrl: String?,
        @field:SerializedName("keys_url")
        val keysUrl: String?,
        @field:SerializedName("collaborators_url")
        val collaboratorsUrl: String?,
        @field:SerializedName("teams_url")
        val teamsUrl: String?,
        @field:SerializedName("hooks_url")
        val hooksUrl: String?,
        @field:SerializedName("issue_events_url")
        val issueEventsUrl: String?,
        @field:SerializedName("events_url")
        val eventsUrl: String?,
        @field:SerializedName("assignees_url")
        val assigneesUrl: String?,
        @field:SerializedName("branches_url")
        val branchesUrl: String?,
        @field:SerializedName("tags_url")
        val tagsUrl: String?,
        @field:SerializedName("blobs_url")
        val blobsUrl: String?,
        @field:SerializedName("git_tags_url")
        val gitTagsUrl: String?,
        @field:SerializedName("git_refs_url")
        val gitRefsUrl: String?,
        @field:SerializedName("trees_url")
        val treesUrl: String?,
        @field:SerializedName("statuses_url")
        val statusesUrl: String?,
        @field:SerializedName("languages_url")
        val languagesUrl: String?,
        @field:SerializedName("stargazers_url")
        val stargazersUrl: String?,
        @field:SerializedName("contributors_url")
        val contributorsUrl: String?,
        @field:SerializedName("subscribers_url")
        val subscribersUrl: String?,
        @field:SerializedName("subscription_url")
        val subscriptionUrl: String?,
        @field:SerializedName("commits_url")
        val commitsUrl: String?,
        @field:SerializedName("git_commits_url")
        val gitCommitsUrl: String?,
        @field:SerializedName("comments_url")
        val commentsUrl: String?,
        @field:SerializedName("issue_comment_url")
        val issueCommentUrl: String?,
        @field:SerializedName("contents_url")
        val contentsUrl: String?,
        @field:SerializedName("compare_url")
        val compareUrl: String?,
        @field:SerializedName("merges_url")
        val mergesUrl: String?,
        @field:SerializedName("archive_url")
        val archiveUrl: String?,
        @field:SerializedName("downloads_url")
        val downloadsUrl: String?,
        @field:SerializedName("issues_url")
        val issuesUrl: String?,
        @field:SerializedName("pulls_url")
        val pullsUrl: String?,
        @field:SerializedName("milestones_url")
        val milestonesUrl: String?,
        @field:SerializedName("notifications_url")
        val notificationsUrl: String?,
        @field:SerializedName("labels_url")
        val labelsUrl: String?,
        @field:SerializedName("releases_url")
        val releasesUrl: String?,
        @field:SerializedName("deployments_url")
        val deploymentsUrl: String?,
        @field:SerializedName("created_at")
        val createdAt: String?,
        @field:SerializedName("updated_at")
        val updatedAt: String?,
        @field:SerializedName("pushed_at")
        val pushedAt: String?,
        @field:SerializedName("git_url")
        val gitUrl: String?,
        @field:SerializedName("ssh_url")
        val sshUrl: String?,
        @field:SerializedName("clone_url")
        val cloneUrl: String?,
        @field:SerializedName("svn_url")
        val svnUrl: String?,
        @field:SerializedName("homepage")
        val homepage: String?,
        @field:SerializedName("size")
        val size: Int,
        @field:SerializedName("watchers_count")
        val watchersCount: Int,
        @field:SerializedName("language")
        val language: String?,
        @field:SerializedName("has_issues")
        val hasIssues: Boolean,
        @field:SerializedName("has_projects")
        val hasProjects: Boolean,
        @field:SerializedName("has_downloads")
        val hasDownloads: Boolean,
        @field:SerializedName("has_wiki")
        val hasWiki: Boolean,
        @field:SerializedName("has_pages")
        val hasPages: Boolean,
        @field:SerializedName("forks_count")
        val forksCount: Int,
        @field:SerializedName("archived")
        val isArchived: Boolean,
        @field:SerializedName("disabled")
        val isDisabled: Boolean,
        @field:SerializedName("open_issues_count")
        val openIssuesCount: Int,
        @field:SerializedName("forks")
        val forks: Int,
        @field:SerializedName("open_issues")
        val openIssues: Int,
        @field:SerializedName("watchers")
        val watchers: Int,
        @field:SerializedName("default_branch")
        val defaultBranch: String?,
        @field:SerializedName("score")
        val score: Double,
        @field:SerializedName("mirror_url")
        val mirrorUrl: String?,
        @Embedded
        @field:SerializedName("owner")
        val owner: Owner,
        @Embedded
        @field:SerializedName("license")
        val license: License?) {
}

data class Owner(
        @field:SerializedName("login")
        val login: String?,
        @field:SerializedName("id")
        val ownerId: Int,
        @field:SerializedName("node_id")
        val ownerNodeId: String?,
        @field:SerializedName("avatar_url")
        val avatarUrl: String?,
        @field:SerializedName("gravatar_id")
        val gravatarId: String?,
        @field:SerializedName("url")
        val ownerUrl: String?,
        @field:SerializedName("html_url")
        val ownerHtmlUrl: String?,
        @field:SerializedName("followers_url")
        val followersUrl: String?,
        @field:SerializedName("following_url")
        val followingUrl: String?,
        @field:SerializedName("gists_url")
        val gistsUrl: String?,
        @field:SerializedName("starred_url")
        val starredUrl: String?,
        @field:SerializedName("subscriptions_url")
        val subscriptionsUrl: String?,
        @field:SerializedName("organizations_url")
        val organizationsUrl: String?,
        @field:SerializedName("repos_url")
        val reposUrl: String?,
        @field:SerializedName("events_url")
        val ownerEventsUrl: String?,
        @field:SerializedName("received_events_url")
        val receivedEventsUrl: String?,
        @field:SerializedName("type")
        val type: String?,
        @field:SerializedName("site_admin")
        val isSiteAdmin: Boolean)

data class License(
        @field:SerializedName("key")
        val key: String,
        @field:SerializedName("name")
        val licenseName: String?,
        @field:SerializedName("spdx_id")
        val spdxId: String?,
        @field:SerializedName("url")
        val licenseUrl: String?,
        @field:SerializedName("node_id")
        val licenseNodeId: String?)

/**
 * Decrypt encrypted data
 */
fun GitRepo.decrypt() = this.copy(
        name = CryptoTool.decrypt(this.name),
        htmlUrl = CryptoTool.decrypt(this.htmlUrl),
        language = this.language?.let { CryptoTool.decrypt(this.language) })


/**
 * Encrypt important data
 */
fun GitRepo.encrypt() = this.copy(
        name = CryptoTool.encrypt(this.name),
        htmlUrl = CryptoTool.encrypt(this.htmlUrl),
        language = this.language?.let { CryptoTool.encrypt(this.language) })


