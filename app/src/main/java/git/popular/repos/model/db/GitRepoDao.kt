package git.popular.repos.model.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import git.popular.repos.model.entity.GitRepo


/**
 * The Data Access Object for the GitRepo class.
 */
@Dao
interface GitRepoDao {

    @Query("SELECT * FROM gitRepos ORDER BY stargazersCount DESC")
    fun getPagedGitRepos(): DataSource.Factory<Int, GitRepo>

    @Query("SELECT * FROM gitRepos WHERE id = :id")
    fun getGitRepo(id: String): LiveData<GitRepo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(gitRepos: List<GitRepo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(gitRepo: GitRepo)

    @Query("DELETE FROM gitRepos")
    suspend fun deleteAll()
}
