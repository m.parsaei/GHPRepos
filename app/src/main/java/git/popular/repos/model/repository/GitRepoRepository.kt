package git.popular.repos.model.repository

import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import git.popular.repos.model.datasource.GitRepoPageDataSourceFactory
import git.popular.repos.model.datasource.GitRepoRemoteDataSource
import git.popular.repos.model.db.GitRepoDao
import git.popular.repos.model.db.Result
import git.popular.repos.model.db.resultLiveData
import git.popular.repos.model.entity.GitRepo
import git.popular.repos.model.entity.decrypt
import git.popular.repos.util.CryptoTool.isBiometricAuthenticated
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository module for handling data operations.
 */
@Singleton
class GitRepoRepository @Inject constructor(private val dao: GitRepoDao,
                                            private val remoteSource: GitRepoRemoteDataSource) {

    fun getPagedGitRepos(connectivityAvailable: Boolean,
                         coroutineScope: CoroutineScope) =
            if (connectivityAvailable) getRemotePagedGitRepos(coroutineScope)
            else getLocalPagedGitRepos()

    private fun getLocalPagedGitRepos(): LiveData<PagedList<GitRepo>> {
        val dataSourceFactory = dao.getPagedGitRepos()

        return LivePagedListBuilder(dataSourceFactory.map { it.decrypt() },
                GitRepoPageDataSourceFactory.pagedListConfig()).build()
    }

    private fun getRemotePagedGitRepos(ioCoroutineScope: CoroutineScope)
            : LiveData<PagedList<GitRepo>> {
        val dataSourceFactory = GitRepoPageDataSourceFactory(remoteSource,
                dao, ioCoroutineScope)
        return  LivePagedListBuilder(dataSourceFactory,
                GitRepoPageDataSourceFactory.pagedListConfig()).build()
    }

    fun getGitRepo(id: String): LiveData<Result<GitRepo>> {
        return if (isBiometricAuthenticated) {
            resultLiveData(
                    databaseQuery = { dao.getGitRepo(id).map { it.decrypt() } },
                    networkCall = { remoteSource.fetchGitRepo(id) },
                    saveCallResult = { dao.insert(it) })
                    .distinctUntilChanged()
        } else {
            liveData(Dispatchers.IO) {
                emit(Result.loading())
                val responseStatus = remoteSource.fetchGitRepo(id)
                if (responseStatus.status == Result.Status.SUCCESS) {
                    emit(Result.success(responseStatus.data!!))
                }
            }
        }
    }

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: GitRepoRepository? = null

        fun getInstance(dao: GitRepoDao, remoteSource: GitRepoRemoteDataSource) =
                instance
                        ?: synchronized(this) {
                            instance
                                    ?: GitRepoRepository(dao, remoteSource).also { instance = it }
                        }
    }
}
