package git.popular.repos.model.datasource

import git.popular.repos.model.api.GitService
import javax.inject.Inject

/**
 * Works with the GitHub API to get data.
 */
class GitRepoRemoteDataSource @Inject constructor(private val service: GitService) : BaseDataSource() {

    suspend fun fetchGitRepos(page: Int, pageSize: Int? = null)
            = getResult { service.getGitRepositories(page= page,pageSize =  pageSize) }

    suspend fun fetchGitRepo(id: String)
            = getResult { service.getGitRepository(id) }
}
