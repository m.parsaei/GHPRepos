package git.popular.repos.model.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import git.popular.repos.model.entity.GitRepo
import git.popular.repos.model.db.GitRepoDao
import git.popular.repos.model.db.Result
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class GitRepoPageDataSourceFactory @Inject constructor(
        private val remoteSource: GitRepoRemoteDataSource,
        private val dao: GitRepoDao,
        private val scope: CoroutineScope) :  DataSource.Factory<Int, GitRepo>() {



    private val liveData = MutableLiveData<GitRepoPageDataSource>()

    override fun create(): DataSource<Int, GitRepo> {
        val source = GitRepoPageDataSource(remoteSource, dao, scope)
        liveData.postValue(source)
        return source
    }

    companion object {
        private const val PAGE_SIZE = 100

        fun pagedListConfig() = PagedList.Config.Builder()
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setPageSize(PAGE_SIZE)
                .setEnablePlaceholders(true)
                .build()
    }
}