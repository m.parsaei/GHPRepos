package git.popular.repos.model.entity

data class User(
        val email: String?,
        val password: String?)