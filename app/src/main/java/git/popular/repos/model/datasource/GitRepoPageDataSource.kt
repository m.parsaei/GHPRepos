package git.popular.repos.model.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import git.popular.repos.model.db.Result
import git.popular.repos.model.entity.GitRepo
import git.popular.repos.model.db.GitRepoDao
import git.popular.repos.model.entity.encrypt
import git.popular.repos.util.CryptoTool.isBiometricAuthenticated
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * Data source for GitHub Repositories pagination via paging library
 */
class GitRepoPageDataSource @Inject constructor(
        private val remoteSource: GitRepoRemoteDataSource,
        private val dao: GitRepoDao,
        private val scope: CoroutineScope) : PageKeyedDataSource<Int, GitRepo>() {



    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, GitRepo>) {
        fetchData(1, params.requestedLoadSize) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, GitRepo>) {
        val page = params.key
        fetchData(page, params.requestedLoadSize) {
            callback.onResult(it, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, GitRepo>) {
        val page = params.key
        fetchData(page, params.requestedLoadSize) {
            callback.onResult(it, page - 1)
        }
    }

    private fun fetchData(page: Int, pageSize: Int, callback: (List<GitRepo>) -> Unit) {

        scope.launch(getJobErrorHandler()) {

            val response = remoteSource.fetchGitRepos(page, pageSize)
            if (response.status == Result.Status.SUCCESS) {
                val results = response.data!!.items

                if (isBiometricAuthenticated)
                    dao.insertAll(results.map { it.encrypt() })


                callback(results)
            } else if (response.status == Result.Status.ERROR) {
                postError(response.message!!)
            }
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        postError(e.message ?: e.toString())
    }

    private fun postError(message: String) {
        Timber.e("An error happened: $message")
        // TODO network error handling
    }

}
