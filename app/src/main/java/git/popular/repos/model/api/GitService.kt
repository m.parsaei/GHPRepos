package git.popular.repos.model.api

import git.popular.repos.model.entity.GitRepo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * GitHub REST API access points
 */
interface GitService {

    companion object {
       const val ENDPOINT = "https://api.github.com/"
    }

    // Sample Url : https://api.github.com/search/repositories?q=language:kotlin&sort=stars&order=desc&per_page=50000&page=100&page=3
    @GET("search/repositories")
    suspend fun getGitRepositories(
                                @Query("q") query: String? = "language:kotlin",
                                @Query("page") page: Int? = null,
                                @Query("per_page") pageSize: Int? = null,
                                @Query("order") sortOrder: String? = "desc",
                                @Query("sort") sortBy: String? = "stars"): Response<ResultsResponse<GitRepo>>

    // Sample Url : https://api.github.com/repositories/20633049
    @GET("repositories/{id}")
    suspend fun getGitRepository(@Path("id") id: String): Response<GitRepo>
}
