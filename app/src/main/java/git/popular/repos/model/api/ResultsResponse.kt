package git.popular.repos.model.api

import com.google.gson.annotations.SerializedName

data class ResultsResponse<T>(
    @SerializedName("total_count")
    val totalCount: Int,
    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,
    @SerializedName("items")
    val items: List<T>
)