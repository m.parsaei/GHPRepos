package git.popular.repos.repository

import git.popular.repos.model.api.GitService
import git.popular.repos.model.datasource.GitRepoRemoteDataSource
import git.popular.repos.model.db.AppDatabase
import git.popular.repos.model.db.GitRepoDao
import git.popular.repos.model.repository.GitRepoRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

@RunWith(JUnit4::class)
class GitRepoRepositoryTest {

    private val service = mock(GitService::class.java)
    private val remoteDataSource = GitRepoRemoteDataSource(service)
    private val dao = mock(GitRepoDao::class.java)
    private lateinit var repository : GitRepoRepository
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    @Before
    fun init(){
        val db = mock(AppDatabase::class.java)
        `when`(db.gitRepoDao()).thenReturn(dao)
        `when`(db.runInTransaction(ArgumentMatchers.any())).thenCallRealMethod()

        repository = GitRepoRepository(dao,remoteDataSource)
    }


    @Test
    fun fetchGitReopsFromNetwork(){
        runBlocking {
            repository.getPagedGitRepos(true,coroutineScope)

            verify(dao, never()).getPagedGitRepos()
            verifyZeroInteractions(dao)
        }
    }

}
