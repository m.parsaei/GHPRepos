package git.popular.repos.api

import git.popular.repos.model.api.GitService
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class GitServiceTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: GitService

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
                .baseUrl(mockWebServer.url(""))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GitService::class.java)
    }

    @Test
    fun getGitRepositories() {
        runBlocking {
            enqueueResponse("repos.json")
            val resultsResponse = service.getGitRepositories().body()
            val repos = resultsResponse!!.items

            Assert.assertEquals(resultsResponse.totalCount,7146042)
            Assert.assertEquals(repos.size,2)
        }
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
                .getResourceAsStream("api-response/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(mockResponse.setBody(source.readString(Charsets.UTF_8)))
    }


    @After
    fun stopService() {
        mockWebServer.shutdown()
    }
}